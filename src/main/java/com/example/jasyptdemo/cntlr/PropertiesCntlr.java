package com.example.jasyptdemo.cntlr;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class PropertiesCntlr {
	@Autowired
	private Environment environment;

	@GetMapping("/password")
	public String getProperty() {
		return environment.getProperty("app.password") + "   " + environment.getProperty("app.clientsecret");
	}

}
