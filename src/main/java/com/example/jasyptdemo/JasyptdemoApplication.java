package com.example.jasyptdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JasyptdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(JasyptdemoApplication.class, args);
	}

}
