package com.example.jasyptdemo.cnfg;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
public class Cnfg {
	@Value("${app.password}")
	private String password;
	@Value("${app.clientsecret}")
	private String clientSecret;

}
